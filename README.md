# Task2

## Prerequisites
In this task, I developed APIs with Golang and MySql as Database. Therefore, before running this, your machine must set up Golang and MySql (Ver 8.1.0) as Database first.

- For Golang, please visit this and follow its steps to install: <https://go.dev/doc/install>

- For Database, firstly, you must install MySql (Ver 8.1.0 or later). Then, create a Database within a table name posts.


## Run

Clone this project and do following steps:

1. You need set up Database first. Create a Database with a table `posts`. Then adding environment files with variables as below example.

* Notice: because I implemented soft deletion instead of hard deletion, you need to add a field/row named `status`

```
DB_USER=root
DB_PWD=
DB_ADDRESS=127.0.0.1
DB_PORT=3306
DB_NAME=Kontinentalist

```
- variables meaning:
    - DB_USER: user of MySql database
    - DB_PWD: database password
    - DB_ADDRESS: database address
    - DB_PORT: database port
    - DB_NAME: database name

- At default, gin server run on port 8080
2. Start the database
3. Run the command

```
go mod tidy
```

 This command ensures that the go.mod file contains the correct dependencies and versions used in your codebase. 

4. Run the following command to run server and test by postman (or any similar tool to test api).

```
go run main.go
```

## APIS brief

I versionized the APIs, so each endpoint starting with name of version
 
|                  **uri**                 | **Method** | **Operation** |                     **Description**                     | **Status** |   **Resquest Body**   |        **Response body**       |
|:----------------------------------------:|:----------:|:-------------:|:-------------------------------------------------------:|:----------:|:---------------------:|:------------------------------:|
| /v1/post/[:id]                        | GET        | Read          | Get a post by id                                     | Available       | N/A                   | a json format of data          |
| /v1/post/                             | POST       | Create        | Create a new post                                    | Available  | a json format of data | code of response               |
| /v1/post/[:id]                        | PATH       | Update        | Update a post by id                                  | Available        | a json format of data | code of response               |
| /v1/post/?page=[number]&lmit=[number] | GET        | Read          | Get a list of post according to page and limit param | Available        | N/A                   | a json format of array of data |
| /v1/post/[:id]                        | DELETE     | Delete        | Delete a post by id (this is soft deletion)

