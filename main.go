package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"kontinentalist-task2/modules/post/transport/ginpost"
)


func main() {
	loadEnv()
	c := connectDB()
	startGinServer(c)

}

func loadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func connectDB() *gorm.DB {
	dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
	dsn = getDBString()
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Connect to database successfully", &db)
	return db
}

func getDBString() string {

	db_user := os.Getenv("DB_USER")
	db_pwd := os.Getenv("DB_PWD")
	db_Add := os.Getenv("DB_ADDRESS")
	db_port := os.Getenv("DB_PORT")
	db_name := os.Getenv("DB_NAME")
	dbStr := db_user + ":" + db_pwd + "@tcp(" + db_Add + ":" + db_port + ")/" + db_name + "?charset=utf8mb4&parseTime=True&loc=Local"
	return dbStr
}

func startGinServer(database *gorm.DB) {
	r := gin.Default()
	v1 := r.Group("/v1")
	posts := v1.Group("/posts")
	/*  [POST]  create a post /v1/posts */
	posts.POST("/", ginpost.CreateStudent(database))
	
	/* GET list of posts */
	posts.GET("/", ginpost.ListPost(database))
	
	/* GET one /v1/postss /:id */
	posts.GET("/:id", ginpost.GetPostByID(database))

	/* PATCH one /v1/postss /:id */
	posts.PATCH("/:id", ginpost.UpdatePost(database))

	/*[DELETE] This is soft Delete by id */
	posts.DELETE("/:id", ginpost.DeletePostByID(database))

	r.Run()


}
