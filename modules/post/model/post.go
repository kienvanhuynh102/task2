package postmodel

type Post struct {
	Id int `json:"id" gorm:"column:id"`
	Title string `json:"title" gorm:"column:title"`
	Content string `json:"content" gorm:"column:content"`
	Status int `json:"status" gorm:"column:status"`

}

func (Post) TableName() string { return "posts" }

type PostCreate struct {
	Id int `json:"id" gorm:"column:id"`
	Title string `json:"title" gorm:"column:title"`
	Content string `json:"content" gorm:"column:content"`
}

func (PostCreate) TableName() string { return Post{}.TableName()}

type PostUpdate struct{
	Title string `json:"title" gorm:"column:title"`
	Content string `json:"content" gorm:"column:content"`
	Status string `json:"status" gorm:"column:status"`
}
func (PostUpdate) TableName() string { return Post{}.TableName()}