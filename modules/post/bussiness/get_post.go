package postbussiness 

import (
	"context"
	Postmodel "kontinentalist-task2/modules/post/model"
)

type GetPostStore interface {
	FindDataWithCondition(ctx context.Context, 
		condition map[string]interface{},
		moreKeys ...string,
		) (*Postmodel.Post, error)
}

type getPostBiz struct{
	store GetPostStore
}

func NewGetPostBiz(store GetPostStore) *getPostBiz {
	return &getPostBiz{store: store}
}

func (biz *getPostBiz) GetPost(ctx context.Context, id int) (*Postmodel.Post, error) {

	Post, err := biz.store.FindDataWithCondition(ctx,map[string]interface{}{"id": id})
	
	if err!= nil {
        return nil, err
    }


	return Post,nil
}