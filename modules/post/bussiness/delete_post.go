package postbussiness


import (
	"context"
	"errors"
	Postmodel "kontinentalist-task2/modules/post/model"
)

type DeletePostStore interface{
	FindDataWithCondition(ctx context.Context, 
		condition map[string]interface{},
		moreKeys ...string,
		) (*Postmodel.Post, error)
	Delete(ctx context.Context, id int) error
}

type deletePostBiz struct {
	store DeletePostStore
}

func NewDeletePostBiz(store DeletePostStore) *deletePostBiz {
	return &deletePostBiz{store: store}
}


func (b *deletePostBiz) DeletePost(ctx context.Context, id int) error {
	oldPost, err := b.store.FindDataWithCondition(ctx,map[string]interface{}{"id": id})
	if err!= nil {
        return err
    }

	if oldPost.Status == 0{
		return errors.New(" post does not exist")
	}

	if err := b.store.Delete(ctx, id); err != nil {
		return err
	}
	return nil
}