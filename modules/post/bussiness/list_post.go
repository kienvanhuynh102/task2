package postbussiness

import (
	"context"
	Postmodel "kontinentalist-task2/modules/post/model"
)

type ListPostStore interface {
	List(ctx context.Context, paging Postmodel.Paging)  ([]Postmodel.Post,error)
}

type listPostBiz struct{
	store ListPostStore
}

func NewListPostBiz(store ListPostStore) *listPostBiz {
	return &listPostBiz{store: store}
}

func (b *listPostBiz) ListPost(ctx context.Context, paging Postmodel.Paging) ([]Postmodel.Post, error) {
	if paging.Page < 0{
		paging.Page = 1
	}
	if paging.Limit < 0{
        paging.Limit = 5
    }
	data, err := b.store.List(ctx, paging)
	if err != nil {
        return nil, err
    }

	return data, nil
}