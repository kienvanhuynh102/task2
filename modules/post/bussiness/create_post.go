package postbussiness

import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
)

type CreatePostStore interface {
	Create(ctx context.Context, data *postmodel.PostCreate) error
}

type createPostBiz struct{
	store CreatePostStore
}

func NewCreatePostBiz(data CreatePostStore) *createPostBiz {
	return &createPostBiz{store: data}
}

func (biz *createPostBiz) CreatePost(ctx context.Context, data *postmodel.PostCreate) error {
   
	
	if err := biz.store.Create(ctx, data); err != nil {
	    return err
    }

	return nil
}