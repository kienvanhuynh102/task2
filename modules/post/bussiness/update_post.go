package postbussiness

import (
	"context"
	Postmodel "kontinentalist-task2/modules/post/model"
	// Postmodel "first-apis/module/Post/model"
)

type UpdatePostStore interface {
	Update(ctx context.Context, id int, data *Postmodel.PostUpdate)  error
}

type updatePostBiz struct{
	store UpdatePostStore
}

func NewUpdatePostBiz(store UpdatePostStore) *updatePostBiz {
	return &updatePostBiz{store: store}
}

func (biz *updatePostBiz) UpdatePost(ctx context.Context, id int, data *Postmodel.PostUpdate) error {

	err := biz.store.Update(ctx, id, data)
	if err!= nil {
        return err
    }

	return nil
}