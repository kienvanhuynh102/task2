package postrepository

import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *postmodel.PostUpdate)  error{
	if err := s.db.Table(postmodel.Post{}.TableName()).
	Where("id = ?", id).
	Updates(&data).Error; err != nil {
		return err
    }	
	return  nil
}