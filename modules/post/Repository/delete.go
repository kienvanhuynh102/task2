package postrepository

import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
)

func (s *sqlStore) Delete(ctx context.Context, id int)  error {
	if err := s.db.Table(postmodel.Post{}.TableName()).
	Where("id = ?", id).
	Updates(map[string]interface{}{"status":0}).Error; err != nil {
		return err
    }	
	return nil
}