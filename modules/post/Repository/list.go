package postrepository


import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
	
)

func (s *sqlStore) List(ctx context.Context, paging postmodel.Paging)  ([]postmodel.Post, error) {
	var data []postmodel.Post
	
	if err := s.db.
	Offset((paging.Page - 1)*paging.Limit).
	Order("id desc").
	Limit(paging.Limit).
	Find(&data).Error; err != nil {
		return nil,err
	}
	
	return data,nil
}