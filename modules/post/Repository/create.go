package postrepository

import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
)

func (s *sqlStore) Create(ctx context.Context, data *postmodel.PostCreate) error{
	if err := s.db.Create(&data).Error; err != nil{
		return err
	}
	return nil
}

