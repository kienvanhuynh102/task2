package postrepository

import (
	"context"
	postmodel "kontinentalist-task2/modules/post/model"
)

func (s *sqlStore) FindDataWithCondition(ctx context.Context, 
	condition map[string]interface{},
	moreKeys ...string,
	) (*postmodel.Post, error){
		
		var data postmodel.Post
		if err := s.db.Where(condition).First(&data).Error; err != nil{
			return nil,err
		}
	return &data,nil
}