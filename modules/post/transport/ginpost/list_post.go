package ginpost

import (

	// "strconv"
	postrepository "kontinentalist-task2/modules/post/Repository"
	postbussiness "kontinentalist-task2/modules/post/bussiness"
	Postmodel "kontinentalist-task2/modules/post/model"

	"net/http"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)


func ListPost(db *gorm.DB) func(c *gin.Context){
	return func(c *gin.Context){
		
		var paging Postmodel.Paging

		if err := c.ShouldBind(&paging); err != nil{
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		var data []Postmodel.Post

		store := postrepository.NewSqlStore(db)
		biz := postbussiness.NewListPostBiz(store)
		data, err := biz.ListPost(c.Request.Context(), paging);
		if  err != nil{
			c.JSON(http.StatusBadRequest, gin.H{
                "message": err.Error(),
            })

            return
        }

		c.JSON(http.StatusOK, gin.H{
			"data" : data,
		})
	}
}