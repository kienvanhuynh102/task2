package ginpost

import (
	postrepository "kontinentalist-task2/modules/post/Repository"
	postbussiness "kontinentalist-task2/modules/post/bussiness"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func DeletePostByID(db *gorm.DB) func(c *gin.Context) {
	return func(c *gin.Context) {

		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
		}

		store := postrepository.NewSqlStore(db)
		biz := postbussiness.NewDeletePostBiz(store)
		if err := biz.DeletePost(c.Request.Context(), id); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"data": "success",
		})
	}
}
