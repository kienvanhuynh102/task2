package ginpost

import (
	postrepository "kontinentalist-task2/modules/post/Repository"
	postbussiness "kontinentalist-task2/modules/post/bussiness"
	postmodel "kontinentalist-task2/modules/post/model"

	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func CreateStudent(db *gorm.DB) func(c *gin.Context){
	return func(c *gin.Context){
		
		var data postmodel.PostCreate
		if err := c.ShouldBind(&data); err != nil{
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			
			return
		}

		// db.Create(&data)
		store := postrepository.NewSqlStore(db)
		biz := postbussiness.NewCreatePostBiz(store)
		if err := biz.CreatePost(c.Request.Context(), &data); err!= nil{
			c.JSON(http.StatusBadRequest, gin.H{
                "message": err.Error(),
            })

            return
        }

		c.JSON(http.StatusOK, gin.H{
			"data" : data,
		})
	}
}