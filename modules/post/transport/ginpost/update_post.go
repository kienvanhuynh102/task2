package ginpost

import (
	postrepository "kontinentalist-task2/modules/post/Repository"
	postbussiness "kontinentalist-task2/modules/post/bussiness"
	postmodel "kontinentalist-task2/modules/post/model"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)


func UpdatePost(db *gorm.DB) func(c *gin.Context){
	return func(c *gin.Context){
		
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil{
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
		}
		var data postmodel.PostUpdate
		if err := c.ShouldBind(&data); err != nil{
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}

		store := postrepository.NewSqlStore(db)
		biz := postbussiness.NewUpdatePostBiz(store)
		if err := biz.UpdatePost(c.Request.Context(), id, &data); err!= nil{
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
		})

		return
	}

		c.JSON(http.StatusOK, gin.H{
			"data" : data,
		})
	}
}